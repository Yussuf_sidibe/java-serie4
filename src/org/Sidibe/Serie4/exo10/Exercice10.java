package org.Sidibe.Serie4.exo10;

import java.util.function.Function;
import java.util.function.BiFunction;
public class Exercice10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Function<String, String> chaineMajuscule = s -> s != null ? s.toUpperCase() : "";
		System.out.println("julio en majuscule :" + chaineMajuscule.apply("julio"));
		
		Function<String, String> memeChaine = s -> s == null ? "" : s;
		System.out.println("Pour une chaine null :" + memeChaine.apply(null));
		System.out.println("Pour une chaine non null : " + memeChaine.apply("youma"));
		
		Function<String, Integer> longueurChaine = s -> s == null ? 0 : s.length();
		System.out.println("longueur de la chaine 'papa' :" + longueurChaine.apply("papa"));
		
		Function<String, String> nouvelleChaine = s -> "(" + s + ")"; 
		System.out.println("la chaine 'papa' entre parenthese :" + nouvelleChaine.apply("papa"));
		System.out.println("la chaine 'vide' entre parenthese :" + nouvelleChaine.apply(""));
		
		BiFunction<String, String, Integer> positionChaine = (s,t) -> s !=null && t !=null ? s.indexOf(t) : -1;
		System.out.println("position de la chaine 'nj' dans 'bonjour' : "+positionChaine.apply("bonjour", "nj"));
		
		Function<String, Integer> positionChaineBis = s -> s !=null ? s.indexOf("abcdefghi") : -1;
		System.out.println("position de la chaine 'de' dans 'adcdefghi.' : "+positionChaineBis.apply("de"));
		System.out.println("position de la chaine 'nj' dans 'adcdefghi.' : "+positionChaineBis.apply("nj"));
	}

}
