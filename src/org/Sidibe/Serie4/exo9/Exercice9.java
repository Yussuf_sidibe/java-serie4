package org.Sidibe.Serie4.exo9;

import java.util.function.Predicate;

public class Exercice9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// predicat qui teste si une chaine de caracteres fait plus de 4
		// caracteres
		Predicate<String> p1 = s -> s != null && s.length() > 4 ? true : false;
		System.out.println("La chaine null fait plus de 4 caracteres : " + p1.test(null));
		System.out.println("La chaine 'abc' fait plus de 4 caracteres :  " + p1.test("abc"));
		System.out.println("La chaine 'yussuf' fait plus de 4 caracteres : " + p1.test("yussuf"));
		 //predicat qui teste si une chaine de caracteres est vide
		Predicate<String> p2 = s -> s != null && s.length() > 0 ? true : false;
		System.out.println("La chaine null est non vide : " + p2.test(null));
		System.out.println("La chaine vide est non vide : " + p2.test(""));
		System.out.println("La chaine 'yussuf' est non vide : " + p2.test("yussuf"));
		// Question 3
		Predicate<String> p3 = s -> s != null && (s.startsWith("J") || s.startsWith("j")) ? true : false;
		System.out.println("La chaine null commence par J :  " + p3.test(null));
		System.out.println("La chaine 'JPaumard' commence par J : " + p3.test("JPaumard"));
		System.out.println("La chaine 'yussuf' commence par J : " + p3.test("yussuf"));
		// Question 4
		Predicate<String> p4 = s -> s != null && s.length() == 5 ? true : false;
		System.out.println("La chaine null compte exactement 5 caracteres: " + p4.test(null));
		System.out.println("La chaine 'bus' compte exactement 5 caracteres: " + p4.test("bus"));
		System.out.println("La chaine yussuf compte exactement 5 caracteres:  " + p4.test("yussuf"));
		// Question 5
		Predicate<String> p5 = p3.and(p4);
		System.out.println("La chaine 'Julie' commence par J et compte 5 caracteres: " + p5.test("julie"));
		System.out.println("La chaine 'Juliette' commence par J et compte 5 caracteres: " + p5.test("Juliette"));
		System.out.println("La chaine 'saoul' commence par J et compte 5 caracteres: " + p5.test("saoul"));
	}

}
