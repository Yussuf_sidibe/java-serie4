package org.Sidibe.Serie4.exo11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Exercice11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Comparator<String> plusLongChaine = Comparator.comparingInt(s -> s.length());
		System.out.println("'julio' est plus grand que 'awa' :" + plusLongChaine.compare("julio", "awa"));

		Comparator<Person> compLastName = Comparator.comparing(p -> p.getLastName());
		Comparator<Person> compFirstName = Comparator.comparing(p -> p.getFirstName());
		// test
		Person p1 = new Person("Julio", "NKIKAM", 25);
		Person p2 = new Person("Youma", "TRAORE", 23);
		if (compLastName.compare(p1, p2) > 0)
			System.out.println(p1.getLastName() + " est apr�s " + p2.getLastName());
		else
			System.out.println(p1.getLastName() + " vient avant " + p2.getLastName());

		Comparator<Person> compLastNameThenFirstName = compLastName.thenComparing(compFirstName);
		// test
		Person p3 = new Person("Julio", "NKIKAM", 25);
		Person p4 = new Person("Youma", "NKIKAM", 23);
		if (compLastNameThenFirstName.compare(p3, p4) > 0)
			System.out.println(p3 + " est apr�s " + p4);
		else
			System.out.println(p3 + " vient avant " + p4);

		Comparator<Person> compInverse = compLastNameThenFirstName.reversed();
		// test
		p1 = new Person("yussuf", "sidibe", 12);
		p2 = new Person("yussuf", "zizou", 13);
		if (compInverse.compare(p1, p2) > 0)
			System.out.println(p1 + " est apr�s " + p2);
		else
			System.out.println(p2 + " vient avant " + p1);

		Comparator<Person> nullFin = Comparator.nullsLast(compLastNameThenFirstName);

		List<Person> people = new ArrayList<>();
		people.add(null);
		people.add(new Person("Awa", "COULIBALY", 20));
		people.add(new Person("Youma", "NKIKAM", 22));
		people.add(null);
		people.add(new Person("Julio", "NKIKAM", 24));
		people.add(null);
		people.add(new Person("Moussa", "KONATE", 19));

		System.out.println(people);
		people.sort(nullFin);
		System.out.println(people);

	}

}
